extends Area2D

export (String) var sceneName = "Level 2"

func _process(delta):
	if global.coins == 10:
		$AnimatedSprite.visible = true
		$CollisionShape2D.disabled = false
	else:
		$AnimatedSprite.visible = false
		$CollisionShape2D.disabled = true

func _on_Flag_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
		global.coins = 0
