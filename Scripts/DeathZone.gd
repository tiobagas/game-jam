extends Area2D

export (String) var sceneName = "Level 2"

func _on_DeathZone_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
		global.coins = 0
