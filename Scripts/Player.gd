extends KinematicBody2D

export (int) var speed = 300
export (int) var GRAVITY = 1200
export (int) var jump_speed = -800

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		$AnimatedSprite.play("run")
		$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		$AnimatedSprite.play("run")
		$AnimatedSprite.flip_h = false
	else:
		velocity.x = 0
		$AnimatedSprite.play("idle")
		
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
		
func _process(delta):
	if Input.is_action_just_pressed("ui_left"):
		$SFX.play()
	elif Input.is_action_just_pressed("ui_right"):
		$SFX.play()
	elif Input.is_action_just_pressed("ui_up"):
		$SFXJump.play()
		
	if Input.is_action_just_released("ui_right"):
		$SFX.stop()
	elif Input.is_action_just_released("ui_left"):
		$SFX.stop()

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)